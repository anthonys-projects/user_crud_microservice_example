<?php

namespace App\ValueObjects;

use InvalidArgumentException;

class UserObject
{
    private const REQUIRED_FIELDS = ['username', 'email'];
    private ?string $userName;
    private ?string $firstName;
    private ?string $lastName;
    private ?string $email;

    private function __construct(?string $userName, ?string $firstName, ?string $lastName, ?string $email)
    {
        $this->userName = $userName;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
    }

    public static function fromData(?array $params): self
    {
        if (empty($params)) {
            throw new InvalidArgumentException('"params" must not be empty');
        }

        $missingFields = array_diff(self::REQUIRED_FIELDS, array_keys($params));

        if (!empty($missingFields)) {
            throw new InvalidArgumentException(
                sprintf('%s required: %s given',
                    implode(', ', $missingFields), implode(', ', array_keys($params)))
            );
        }

        if (empty($params['username'])) {
            throw new InvalidArgumentException('The "username" field must be not empty');
        }

        if (empty($params['email'])) {
            throw new InvalidArgumentException('The "email" field must be not empty');
        }

        return new self(
            $params['username'],
            $params['first_name'] ?? null,
            $params['last_name'] ?? null,
            $params['email']);
    }

    public static function forUpdate(?array $params): self
    {
        if (empty($params)) {
            throw new InvalidArgumentException('"params" must not be empty');
        }

        if (empty($params['username'])) {
            throw new InvalidArgumentException('The "username" field must be not empty');
        }

        return new self(
            $params['username'],
            $params['first_name'] ?? null,
            $params['last_name'] ?? null,
            $params['email'] ?? null
        );
    }

    public static function forDelete(?string $userName): self
    {
        if (empty($userName)) {
            throw new InvalidArgumentException('"username" must not be empty');
        }
        return new self($userName, null, null, null);
    }

    public function export(): array
    {
        return [
            'username' => $this->userName,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'email' => $this->email,
        ];
    }

    public function username(): string
    {
        return $this->userName;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function email(): string
    {
        return $this->email;
    }
}