<?php

namespace App\Entity;

use App\Repository\UserRepository;
use App\ValueObjects\UserObject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 30)]
    private ?string $username = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $first_name = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $last_name = null;

    #[ORM\Column(length: 50)]
    private ?string $email = null;

    public function __construct(
        string  $username,
        ?string $firstName,
        ?string $lastName,
        string  $email)
    {
        $this->username = $username;
        $this->first_name = $firstName;
        $this->last_name = $lastName;
        $this->email = $email;
    }

    public static function fromUserObject(UserObject $object): self
    {
        return new self(
            $object->username(),
            $object->firstName(),
            $object->lastName(),
            $object->email(),
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->first_name = $firstName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->last_name = $lastName;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }
}
