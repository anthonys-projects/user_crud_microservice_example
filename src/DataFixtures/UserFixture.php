<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class UserFixture extends Fixture
{

    private Generator $faker;

    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $manager->persist($this->getUser($i));
        }

        $manager->flush();
    }

    private function getUser(int $i): User
    {
        $firstName = 'name' . $i;
        $lastName = 'last_name' . $i;
        $userName = strtolower($firstName) . '.' . strtolower($lastName);
        $email = $userName . '@test.com';

        return new User(
            $userName,
            $firstName,
            $lastName,
            $email,
        );
    }
}
