<?php

namespace App\Repository;

use App\Entity\User;
use App\Repository\Exceptions\InconsistencyDatabaseException;
use App\Repository\Exceptions\NotFoundUserException;
use App\Repository\Exceptions\UserAlreadyExistsException;
use App\ValueObjects\UserObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getTotalCount(): ?int
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getUsersPaginated(int $page, int $pageSize): ?array
    {
        return $this->createQueryBuilder('u')
            ->setFirstResult(($page - 1) * $pageSize)
            ->setMaxResults($pageSize)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function addUser(UserObject $object): void
    {
        $existingUser = $this->findOneByUsername($object->username());

        if ($existingUser) {
            throw new UserAlreadyExistsException(
                sprintf('User %s already exists', $object->username())
            );
        }

        $this->save(User::fromUserObject($object), true);

    }

    public function updateUser(UserObject $object): void
    {
        $user = $this->findOneByUsername($object->username());

        if (!$user) {
            throw new NotFoundUserException(
                sprintf('User %s not found', $object->username())
            );
        }

        if (!empty($object->firstName())) {
            $user->setFirstName($object->firstName());
        }

        if (!empty($object->lastName())) {
            $user->setLastName($object->lastName());
        }

        if (!empty($object->email())) {
            $user->setEmail($object->email());
        }

        $this->save($user, true);
    }

    public function removeUser(UserObject $object): void
    {
        $user = $this->findOneByUsername($object->username());

        if (!$user) {
            throw new NotFoundUserException(
                sprintf('User %s not found', $object->username())
            );
        }

        $this->remove($user, true);
    }

    public function findOneByUsername($username): ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->andWhere('u.username = :val')
                ->setParameter('val', $username)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            throw new InconsistencyDatabaseException($exception->getMessage());
        }
    }

    private function save(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    private function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
