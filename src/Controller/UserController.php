<?php

namespace App\Controller;

use App\ApplicationServices\UserService;
use App\Repository\Exceptions\NotFoundUserException;
use App\Repository\Exceptions\UserAlreadyExistsException;
use App\ValueObjects\UserObject;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class UserController extends AbstractController
{
    public UserService $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    #[Route('/user/list', name: 'get_users', methods: ['GET'])]
    public function listAction(Request $request): JsonResponse
    {
        $page = $request->query->get('page');
        $pageSize = $request->query->get('page_size');

        return new JsonResponse($this->service->list($page, $pageSize));
    }

    #[Route('/user/add', name: 'add_user', methods: ['POST'])]
    public function addAction(Request $request): JsonResponse
    {
        try {
            $this->service->add(UserObject::fromData(
                json_decode($request->getContent(), true)
            ));

            return new JsonResponse('User added');
        } catch (UserAlreadyExistsException $exception) {
            return new JsonResponse($exception->getMessage(), 422);
        } catch (InvalidArgumentException $exception) {
            return new JsonResponse($exception->getMessage(), 400);
        } catch (Throwable $exception) {
            return new JsonResponse($exception->getMessage(), 500);
        }
    }

    #[Route('/user/update', name: 'update_user', methods: ['PATCH'])]
    public function updateAction(Request $request): JsonResponse
    {
        try {
            $this->service->update(UserObject::forUpdate(
                json_decode($request->getContent(), true)
            ));
            return new JsonResponse('User updated');
        } catch (InvalidArgumentException $exception) {
            return new JsonResponse($exception->getMessage(), 400);
        } catch (NotFoundUserException $exception) {
            return new JsonResponse($exception->getMessage(), 404);
        } catch (Throwable $exception) {
            return new JsonResponse($exception->getMessage(), 500);
        }
    }

    #[Route('/user/delete/{username}', name: 'delete_user', methods: ['DELETE'])]
    public function deleteAction(Request $request): JsonResponse
    {
        try {
            $this->service->remove(UserObject::forDelete(
                $request->attributes->get('username')
            ));
            return new JsonResponse('User deleted');
        } catch (NotFoundUserException $exception) {
            return new JsonResponse($exception->getMessage(), 404);
        }

    }
}
