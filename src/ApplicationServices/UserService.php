<?php

namespace App\ApplicationServices;

use App\Repository\UserRepository;
use App\ValueObjects\UserObject;

class UserService
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function list(?int $page = 1, ?int $pageSize = 10): array
    {
        $result = $this->repository->getUsersPaginated($page, $pageSize);

        $totalCount = $this->repository->getTotalCount();
        $totalPageCount = ceil($totalCount / (float)$pageSize);

        return [
            'users' => array_map(function ($item) {
                return UserObject::fromData([
                    'username' => $item['username'],
                    'first_name' => $item['first_name'],
                    'last_name' => $item['last_name'],
                    'email' => $item['email'],
                ])->export();
            }, $result),
            'total_count' => $totalCount,
            'total_page_count' => $totalPageCount,
            'page' => $page,
            'page_size' => $pageSize,
        ];

    }

    public function add(UserObject $object): void
    {
        $this->repository->addUser($object);
    }

    public function update(UserObject $object): void
    {
        $this->repository->updateUser($object);
    }

    public function remove(UserObject $object): void
    {
        $this->repository->removeUser($object);
    }

}