# user_crud_microservice_example

This software is left without any warranty.
It's just an example of microservice written with Symfony.
You can use it and redistribute it.

1. create the database and configure .env.local and .env.local.test accordingly

run migrations (add --env=test for test database)
```
php bin/console doctrine:migrations:migrate
```

run fixtures (add --env=test for test database)
```
php bin/console doctrine:fixtures:load
```

run tests
```
php bin/phpunit
```