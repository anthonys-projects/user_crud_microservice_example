<?php

namespace App\Tests\Unit\ApplicationServices;

use App\ApplicationServices\UserService;
use App\Repository\UserRepository;
use App\ValueObjects\UserObject;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    public function testListSuccessfully(): void
    {
        $repositoryMock = $this->createMock(UserRepository::class);

        $service = new UserService($repositoryMock);

        $user1 = [
            'id' => 123,
            'username' => 'tasmanian.devil',
            'first_name' => 'Devil (Taz)',
            'last_name' => 'Tasmanian',
            'email' => 'tasmanian.devil@acme.com',
        ];
        $user2 = [
            'id' => 456,
            'username' => 'sylvester.pussycat',
            'first_name' => 'Sylvester',
            'last_name' => 'Pussycat',
            'email' => 'sylvester.pussycat@acme.com',
        ];

        $repositoryMock
            ->expects($this->once())
            ->method('getUsersPaginated')
            ->with(1, 10)
            ->willReturn([$user1, $user2]);

        $repositoryMock
            ->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(2);

        $users = $service->list();
        $this->assertEquals([
            'users' => [
                [
                    'username' => 'tasmanian.devil',
                    'first_name' => 'Devil (Taz)',
                    'last_name' => 'Tasmanian',
                    'email' => 'tasmanian.devil@acme.com',
                ], [
                    'username' => 'sylvester.pussycat',
                    'first_name' => 'Sylvester',
                    'last_name' => 'Pussycat',
                    'email' => 'sylvester.pussycat@acme.com',
                ]
            ],
            'page' => 1,
            'page_size' => 10,
            'total_count' => 2,
            'total_page_count' => 1,
        ],
            $users
        );
    }

    public function testAddSuccessfully(): void
    {
        $repositoryMock = $this->createMock(UserRepository::class);
        $service = new UserService($repositoryMock);

        $userObject = UserObject::fromData([
            'username' => 'porky.pig',
            'first_name' => 'Porky',
            'last_name' => 'Pig',
            'email' => 'porky.pig@acme.com'
        ]);

        $repositoryMock
            ->expects($this->once())
            ->method('addUser')
            ->with($userObject);

        $service->add($userObject);
    }

    public function testUpdateSuccessfully(): void
    {
        $repositoryMock = $this->createMock(UserRepository::class);
        $service = new UserService($repositoryMock);

        $userObject = UserObject::fromData([
            'username' => 'porky.pig',
            'first_name' => 'Porky',
            'last_name' => 'Pig',
            'email' => 'porky.pig@acme.com'
        ]);

        $repositoryMock
            ->expects($this->once())
            ->method('updateUser')
            ->with($userObject);

        $service->update($userObject);
    }

    public function testRemoveSuccessfully(): void
    {
        $repositoryMock = $this->createMock(UserRepository::class);
        $service = new UserService($repositoryMock);

        $userObject = UserObject::forDelete('porky.pig');

        $repositoryMock
            ->expects($this->once())
            ->method('removeUser')
            ->with($userObject);

        $service->remove($userObject);
    }
}
