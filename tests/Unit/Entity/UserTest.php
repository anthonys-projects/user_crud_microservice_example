<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use App\ValueObjects\UserObject;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testCreateFromUserObjectSuccessfully(): void
    {
        $user = User::fromUserObject(UserObject::fromData([
            'username' => 'porky.pig',
            'first_name' => 'Porky',
            'last_name' => 'Pig',
            'email' => 'porky.pig@acme.com'
        ]));

        $this->assertInstanceOf(User::class, $user);
    }
}
