<?php

namespace App\Tests\ValueObjects;

use App\ValueObjects\UserObject;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class UserObjectTest extends TestCase
{
    public function testFromParamsSuccessfully(): void
    {
        $user = UserObject::fromData([
            'username' => 'bugs.bunny',
            'first_name' => 'Bugs',
            'last_name' => 'Bunny',
            'email' => 'bugs.bunny@acme.com'
        ]);

        $this->assertEquals([
            'username' => 'bugs.bunny',
            'first_name' => 'Bugs',
            'last_name' => 'Bunny',
            'email' => 'bugs.bunny@acme.com'
        ], $user->export());
    }

    public function testFromParamsWithMandatoryDataOnly(): void
    {
        $user = UserObject::fromData([
            'username' => 'bugs.bunny',
            'email' => 'bugs.bunny@acme.com'
        ]);

        $this->assertEquals([
            'username' => 'bugs.bunny',
            'first_name' => null,
            'last_name' => null,
            'email' => 'bugs.bunny@acme.com'
        ], $user->export());
    }

    public function testForUpdateSuccessfully(): void
    {
        $user = UserObject::forUpdate([
            'username' => 'bugs.bunny',
            'last_name' => 'Bunny',
        ]);

        $this->assertEquals([
            'username' => 'bugs.bunny',
            'first_name' => null,
            'last_name' => 'Bunny',
            'email' => null,
        ], $user->export());
    }


    public function testForDeleteSuccessfully(): void
    {
        $user = UserObject::forDelete('bugs.bunny');
        $this->assertEquals('bugs.bunny', $user->username());
    }

    public function testForDeleteWithEmptyUsernameWillThrowException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('"username" must not be empty');
        UserObject::forDelete('');
    }

    public function testFromParamsWithEmptyDataWillThrowException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('"params" must not be empty');
        UserObject::fromData([]);
    }

    public function testFromParamsWithNullDataWillThrowException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('"params" must not be empty');
        UserObject::fromData(null);
    }

    public function testForUpdateWithEmptyDataWillThrowException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('"params" must not be empty');
        UserObject::forUpdate([]);
    }

    public function testForUpdateWithNullDataWillThrowException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('"params" must not be empty');
        UserObject::forUpdate(null);
    }

    /**
     * @dataProvider invalidData
     */
    public function testFromParamsWithInvalidDataWillThrowException($username, $email, $message): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        UserObject::fromData([
            'username' => $username,
            'first_name' => 'Bugs',
            'last_name' => 'Bunny',
            'email' => $email
        ]);
    }

    /**
     * @dataProvider missingData
     */
    public function testFromParamsWithMissingRequiredFieldsWillThrowException($params, $message): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        UserObject::fromData($params);
    }

    public function invalidData(): array
    {
        return [
            'empty username and valid email' => [
                'username' => '',
                'email' => 'bugs.bunny@acme.com',
                'message' => 'The "username" field must be not empty',
            ],

            'empty email and valid username' => [
                'username' => 'bugs.bunny',
                'email' => '',
                'message' => 'The "email" field must be not empty',
            ],
        ];
    }

    public function missingData(): array
    {
        return [
            'missing username' => [
                'params' => [
                    'first_name' => 'Bugs',
                    'last_name' => 'Bunny',
                    'email' => 'bugs.bunny@acme.com'
                ],
                'message' => 'username required: first_name, last_name, email given',
            ],
            'missing email' => [
                'params' => [
                    'username' => 'bugs.bunny',
                    'first_name' => 'Bugs',
                    'last_name' => 'Bunny',
                ],
                'message' => 'email required: username, first_name, last_name given',
            ],
        ];
    }
}