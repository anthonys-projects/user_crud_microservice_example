<?php

namespace App\Tests\Functional\Controller;

use App\DataFixtures\UserFixture;
use App\Entity\User;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    private $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();

        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $loader = new Loader();
        $loader->addFixture(new UserFixture());

        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    public function testListUsersSuccessfully(): void
    {
        $this->client->request('GET', '/user/list?page=1&page_size=3');

        $responseContent = $this->client->getResponse()->getContent();
        $responseData = json_decode($responseContent, true);

        $this->assertEquals([
            'users' => [
                [
                    'username' => 'name0.last_name0',
                    'first_name' => 'name0',
                    'last_name' => 'last_name0',
                    'email' => 'name0.last_name0@test.com',
                ],
                [
                    'username' => 'name1.last_name1',
                    'first_name' => 'name1',
                    'last_name' => 'last_name1',
                    'email' => 'name1.last_name1@test.com',
                ],
                [
                    'username' => 'name2.last_name2',
                    'first_name' => 'name2',
                    'last_name' => 'last_name2',
                    'email' => 'name2.last_name2@test.com',
                ]
            ],
            'total_count' => 100,
            'total_page_count' => 34,
            'page' => 1,
            'page_size' => 3,
        ], $responseData);
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
    }

    public function testAddUserSuccessfully(): void
    {
        $data = [
            'username' => 'emma.webster',
            'first_name' => 'Emma',
            'last_name' => 'Webster',
            'email' => 'emma.webster@acme.com'
        ];
        $this->client->request('POST', '/user/add', [], [], [], json_encode($data));

        $responseContent = $this->client->getResponse()->getContent();

        $this->assertResponseIsSuccessful();
        $this->assertEquals('"User added"', $responseContent);

        $newUser = $this->entityManager
            ->getRepository(User::class)
            ->findByUsername('emma.webster');
        $newUser = $newUser[0];

        $this->assertEquals($data['username'], $newUser->getUsername());
        $this->assertEquals($data['first_name'], $newUser->getFirstName());
        $this->assertEquals($data['last_name'], $newUser->getLastName());
        $this->assertEquals($data['email'], $newUser->getEmail());
    }

    public function testAddUserWithInvalidDataWillGenerateBadRequestResponse(): void
    {
        $data = [
            'first_name' => 'Emma',
            'last_name' => 'Webster',
            'email' => 'emma.webster@acme.com'
        ];
        $this->client->request('POST', '/user/add', [], [], [], json_encode($data));
        $this->client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(400);
    }

    public function testUpdateUserSuccessfullyWillUpdateOneRow(): void
    {
        $data = [
            'username' => 'name99.last_name99',
            'first_name' => 'NewName',
            'last_name' => 'NewLastName',
            'email' => 'newname.newlastname@acme.com'
        ];

        $existingUser = $this->entityManager
            ->getRepository(User::class)
            ->findByUsername('name99.last_name99');
        $existingUser = $existingUser[0];

        $this->assertEquals('name99.last_name99', $existingUser->getUsername());
        $this->assertEquals('name99', $existingUser->getFirstName());
        $this->assertEquals( 'last_name99', $existingUser->getLastName());
        $this->assertEquals('name99.last_name99@test.com', $existingUser->getEmail());

        $this->client->request('PATCH', '/user/update', [], [], [], json_encode($data));
        $responseContent = $this->client->getResponse()->getContent();

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $updatedUser = $this->entityManager
            ->getRepository(User::class)
            ->findByUsername('name99.last_name99');
        $updatedUser = $updatedUser[0];

        $this->assertEquals($data['username'], $updatedUser->getUsername());
        $this->assertEquals($data['first_name'], $updatedUser->getFirstName());
        $this->assertEquals($data['last_name'], $updatedUser->getLastName());
        $this->assertEquals($data['email'], $updatedUser->getEmail());

        $this->assertEquals('"User updated"', $responseContent);
    }

    public function testUpdateNonExistingUserWillReturn404Response(): void
    {
        $data = [
            'username' => 'not_existing_username',
            'first_name' => 'NewName',
            'last_name' => 'NewLastName',
            'email' => 'newname.newlastname@acme.com'
        ];
        $this->client->request('PATCH', '/user/update', [], [], [], json_encode($data));
        $this->client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(404);
    }

    public function testUpdateWithNoUserNameWillReturn400Response(): void
    {
        $data = [
            'first_name' => 'NewName',
            'last_name' => 'NewLastName',
            'email' => 'newname.newlastname@acme.com'
        ];
        $this->client->request('PATCH', '/user/update', [], [], [], json_encode($data));
        $this->client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(400);
    }

    public function testDeleteUserWillDeleteOneRow(): void
    {
        $this->client->request('DELETE', '/user/delete/name70.last_name70');
        $responseContent = $this->client->getResponse()->getContent();

        $deletedUser = $this->entityManager
            ->getRepository(User::class)
            ->findByUsername('name70.last_name70');

        $this->assertEmpty($deletedUser);
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('"User deleted"', $responseContent);
    }

    public function testDeleteNotExistingUserWillReturn404Response(): void
    {
        $this->client->request('DELETE', '/user/delete/not_existing_user');
        $this->client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(404);
    }
}
