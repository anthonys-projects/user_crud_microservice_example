<?php

namespace App\Tests\Functional\Repository;

use App\DataFixtures\UserFixture;
use App\Entity\User;
use App\Repository\Exceptions\NotFoundUserException;
use App\Repository\Exceptions\UserAlreadyExistsException;
use App\ValueObjects\UserObject;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $loader = new Loader();
        $loader->addFixture(new UserFixture());

        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

    }

    public function testAddUserSuccessfullyWillAddNewRow(): void
    {
        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(100, sizeof($users));

        $this->entityManager
            ->getRepository(User::class)
            ->addUser(UserObject::fromData([
                'username' => 'daffy.duck',
                'first_name' => 'Daffy',
                'last_name' => 'Duck',
                'email' => 'daffy.duck@acme.com'
            ]));

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(101, sizeof($users));
    }

    public function testUpdateUserWillUpdateOneRow(): void
    {
        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(100, sizeof($users));

        $userObject = UserObject::fromData([
            'username' => 'name22.last_name22',
            'first_name' => 'NewBorn',
            'last_name' => 'TwentyTwo',
            'email' => 'newborn.twentytwo@acme.com'
        ]);
        $this->entityManager
            ->getRepository(User::class)
            ->updateUser($userObject);

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(100, sizeof($users));

        $updatedUser = $this->entityManager
            ->getRepository(User::class)
            ->findByUsername('name22.last_name22');
        $updatedUser = $updatedUser[0];

        $this->assertEquals($userObject->username(), $updatedUser->getUsername());
        $this->assertEquals($userObject->firstName(), $updatedUser->getFirstName());
        $this->assertEquals($userObject->lastName(), $updatedUser->getLastName());
        $this->assertEquals($userObject->email(), $updatedUser->getEmail());
    }

    public function testAddUserWithExistingUsernameWillThrowException(): void
    {
        $this->expectException(UserAlreadyExistsException::class);
        $this->expectExceptionMessage('User name84.last_name84 already exists');

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(100, sizeof($users));

        $this->entityManager
            ->getRepository(User::class)
            ->addUser(UserObject::fromData([
                'username' => 'name84.last_name84',
                'first_name' => 'name84',
                'last_name' => 'last_name84',
                'email' => 'name84.last_name84@acme.com'
            ]));

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(101, sizeof($users));
    }

    public function testRemoveUserSuccessfullyWillRemoveOneRow(): void
    {
        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(100, sizeof($users));

        $this->entityManager
            ->getRepository(User::class)
            ->removeUser(UserObject::forDelete('name84.last_name84'));

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(99, sizeof($users));
    }

    public function testRemoveUserWithNotExistingUsernameWillThrowException(): void
    {
        $this->expectException(NotFoundUserException::class);
        $this->expectExceptionMessage('User not_existing_user not found');

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(100, sizeof($users));

        $this->entityManager
            ->getRepository(User::class)
            ->removeUser(UserObject::fromData([
                'username' => 'not_existing_user',
                'first_name' => 'IAm',
                'last_name' => 'NoOne',
                'email' => 'not_existing_user@acme.com'
            ]));

        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAll();

        $this->assertEquals(101, sizeof($users));
    }

    public function testGetUsersPaginatedWithDefaultParams()
    {
        $users = $this->entityManager
            ->getRepository(User::class)
            ->getUsersPaginated(1, 10);

        $this->assertEquals(10, sizeof($users));
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
